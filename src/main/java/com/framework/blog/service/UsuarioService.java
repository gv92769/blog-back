package com.framework.blog.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.framework.blog.entity.PerfilUsuario;
import com.framework.blog.entity.Usuario;
import com.framework.blog.repository.UsuarioRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class UsuarioService {

	@Autowired
    private UsuarioRepository repository;

	public Usuario salvar(Usuario usuario) {
		Set<PerfilUsuario> perfis = new HashSet<>();
		
		perfis.add(PerfilUsuario.ROLE_ADMINISTRADOR);
		perfis.add(PerfilUsuario.ROLE_USUARIO);
		
		usuario.setPerfis(perfis);
		return repository.save(usuario);
	}

	
}
