package com.framework.blog.service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.framework.blog.entity.Album;
import com.framework.blog.repository.AlbumRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class AlbumService {

	private final String raiz = "src/main/resources/";
	
	@Autowired
	AlbumRepository repository;
	
	@Autowired
	FotoService fotoService;
	
	public List<Album> listAll() {
		return repository.findAll();
	}
	
	public Album salvar(Album album) throws IllegalStateException, IOException {
		List<MultipartFile> imagens = album.getImagens();
		album.setFotos(null);
		album = repository.save(album);
		album.setFotos(fotoService.salvarAll(imagens, album.getId()));
		return album;
	}
	
	public void delete(Long id) {
		repository.deleteById(id);
		File f = new File(raiz + id.toString());
		f.delete();
	}
	
}
