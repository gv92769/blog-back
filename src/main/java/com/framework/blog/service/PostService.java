package com.framework.blog.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.framework.blog.entity.Post;
import com.framework.blog.repository.PostRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class PostService {

	@Autowired
	PostRepository repository;
	
	public List<Post> listAll() {
		return repository.findAll();
	}
	
	public Post salvar(Post post) {
		return repository.save(post);
	}
	
	public void delete(Long id) {
		repository.deleteById(id);
	}
	
}
