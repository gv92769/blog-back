package com.framework.blog.service;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.framework.blog.entity.Album;
import com.framework.blog.entity.Foto;
import com.framework.blog.repository.FotoRepository;

import io.jsonwebtoken.io.IOException;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class FotoService {

	private final String raiz = "src/main/resources/";
	
	@Autowired
	FotoRepository repository;
	
	public List<Foto> salvarAll(List<MultipartFile> imagens, Long id) throws IllegalStateException, java.io.IOException {
		List<Foto> fotos = new ArrayList<Foto>();
		
		for (MultipartFile imagem: imagens) {
			Path diretorioPath = Paths.get(this.raiz, id.toString());
			Path arquivoPath = diretorioPath.resolve(imagem.getOriginalFilename());
			
			try {
				Files.createDirectories(diretorioPath);
				imagem.transferTo(arquivoPath.toFile());
				
				Foto foto = new Foto();
				
				foto.setAlbum(new Album(id));
				foto.setDescricao(arquivoPath.toFile().toString());
				
				fotos.add(foto);
			} catch (IOException e) {
				throw new RuntimeException("Erro ao salvar arquivo.", e);
			}		
		}
		
		return repository.saveAll(fotos);
	}
	
}
