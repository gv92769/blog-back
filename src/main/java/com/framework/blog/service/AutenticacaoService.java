package com.framework.blog.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.framework.blog.config.CustomUserDetailsService;
import com.framework.blog.config.JwtTokenProvider;
import com.framework.blog.entity.Usuario;
import com.framework.blog.exception.UnauthorizedExcetion;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class AutenticacaoService {

	private final static String KEY_ACCESS_TOKEN = "access_token";
	
    private final static String KEY_REFRESH_TOKEN = "refresh_token";
    
    @Autowired
    private AuthenticationManager authenticationManager;
    
    @Autowired
    private JwtTokenProvider tokenProvider;
    
    @Autowired
    private CustomUserDetailsService customUserDetailsService;
	
	public Map<String,String> login(Usuario usuario) {
		Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                		usuario.getLogin(),
                		usuario.getSenha()
                )
        );
		
        SecurityContextHolder.getContext().setAuthentication(authentication);
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        
        String accessToken = tokenProvider.createAccessToken(userDetails);
        String refreshToken = tokenProvider.createRefreshToken(userDetails);
        Map<String,String> tokenMap = new HashMap<>();
        tokenMap.put(KEY_ACCESS_TOKEN, accessToken);
        tokenMap.put(KEY_REFRESH_TOKEN, refreshToken);
        
        return tokenMap;
	}
	
	public Map<String,String> refreshToken(String refreshToken) {
		Authentication authentication = null;
        
        try {
            authentication = tokenProvider.parseRefreshToken(refreshToken);
        } catch(ExpiredJwtException e) {
            throw new UnauthorizedExcetion("expired_refresh_token");
        } catch(JwtException e) {
            throw new UnauthorizedExcetion("invalid_refresh_token");
        }
        
        String login = (String) authentication.getPrincipal();
        UserDetails userDetails = customUserDetailsService.loadUserByUsername(login);
        
        if(!userDetails.isEnabled()) {
            throw new UnauthorizedExcetion("user_inactive");
        }
        
        String accessToken = tokenProvider.createAccessToken(userDetails);
        Map<String,String> tokenMap = new HashMap<>();
        tokenMap.put(KEY_ACCESS_TOKEN, accessToken);
        
        return tokenMap;
	}
	
}
