package com.framework.blog.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.framework.blog.entity.Comentario;
import com.framework.blog.repository.ComentarioRepository;

/**
*
* @author Gabriel Vinicius
*/
@Service
public class ComentarioService {

	@Autowired
	ComentarioRepository repository;
	
	public Comentario salvar(Comentario comentario) {
		return repository.save(comentario);
	}
	
	public void delete(Long id) {
		repository.deleteById(id);
	}
	
}
