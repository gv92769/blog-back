package com.framework.blog.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.framework.blog.entity.Album;

/**
*
* @author Gabriel Vinicius
*/
public interface AlbumRepository extends JpaRepository<Album, Long> {

}
