package com.framework.blog.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.framework.blog.entity.Comentario;

/**
*
* @author Gabriel Vinicius
*/
public interface ComentarioRepository extends JpaRepository<Comentario, Long> {

}
