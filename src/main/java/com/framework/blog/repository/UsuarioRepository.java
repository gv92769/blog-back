package com.framework.blog.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.framework.blog.entity.Usuario;

/**
*
* @author Gabriel Vinicius
*/
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
   
	public Optional<Usuario> findByLogin(String login);
   
}
