package com.framework.blog.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.framework.blog.entity.Foto;

/**
*
* @author Gabriel Vinicius
*/
public interface FotoRepository extends JpaRepository<Foto, Long> {

}
