package com.framework.blog.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.framework.blog.entity.Post;

/**
*
* @author Gabriel Vinicius
*/
public interface PostRepository extends JpaRepository<Post, Long> {

}
