package com.framework.blog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.framework.blog.entity.Post;
import com.framework.blog.service.PostService;

/**
*
* @author Gabriel
*/
@RestController
@RequestMapping("/post")
public class PostController {

	@Autowired
	PostService service;
	
	@GetMapping
	public ResponseEntity<List<Post>> listAll() {
		return ResponseEntity.ok().body(service.listAll());
	}
	
	@PostMapping
	public ResponseEntity<Post> salvar(@RequestBody Post post) {
		return ResponseEntity.ok().body(service.salvar(post));
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable("id") Long id) {
		service.delete(id);
	}
	
}
