package com.framework.blog.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.framework.blog.entity.Album;
import com.framework.blog.service.AlbumService;

/**
*
* @author Gabriel Vinicius
*/
@RestController
@RequestMapping("/album")
public class AlbumController {

	@Autowired
	AlbumService service;
	
	@GetMapping
	public ResponseEntity<List<Album>> listAll() {
		return ResponseEntity.ok().body(service.listAll());
	}
	
	@PostMapping
	public ResponseEntity<Album> salvar(@RequestBody Album album) throws IllegalStateException, IOException {
		return ResponseEntity.ok().body(service.salvar(album));
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable("id") Long id) {
		service.delete(id);
	}
	
}
