package com.framework.blog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.framework.blog.entity.Comentario;
import com.framework.blog.service.ComentarioService;

/**
*
* @author Gabriel
*/
@RestController
@RequestMapping("/comentario")
public class ComentarioController {

	@Autowired
	ComentarioService service;
	
	@PostMapping
	public ResponseEntity<Comentario> salvar(@RequestBody Comentario comentario) {
		return ResponseEntity.ok().body(service.salvar(comentario));
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable("id") Long id) {
		service.delete(id);
	}
	
}
