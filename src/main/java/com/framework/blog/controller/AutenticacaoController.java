package com.framework.blog.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.framework.blog.entity.Usuario;
import com.framework.blog.service.AutenticacaoService;

/**
*
* @author Gabriel
*/
@RestController
@RequestMapping("/autenticacao")
public class AutenticacaoController {

	@Autowired
    private AutenticacaoService service;
	
	@PostMapping("/login")
    public ResponseEntity<Map<String,String>> login(@RequestBody Usuario usuario) {
		return ResponseEntity.ok().body(service.login(usuario));
	}
	
	@PostMapping("/refreshtoken")
    public ResponseEntity<Map<String,String>> refreshToken(@RequestBody String refreshToken) {
		return ResponseEntity.ok().body(service.refreshToken(refreshToken));
	}
	
}
