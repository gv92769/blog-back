package com.framework.blog.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

/**
*
* @author Gabriel Vinicius
*/
@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class UnauthorizedExcetion extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9213396247166959427L;

	public UnauthorizedExcetion() {
       super();
    }

	public UnauthorizedExcetion(String message) {
       super(message);
    }

    public UnauthorizedExcetion(String message, Throwable cause) {
       super(message, cause);
    }
   
}
