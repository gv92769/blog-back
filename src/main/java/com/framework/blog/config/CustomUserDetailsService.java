package com.framework.blog.config;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.framework.blog.repository.UsuarioRepository;

/**
*
* @author Gabriel Vinicius
*/
@Component
public class CustomUserDetailsService implements UserDetailsService {

	private final UsuarioRepository repository;

    public CustomUserDetailsService(UsuarioRepository repository) {
        this.repository = repository;
    }
    
    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        return repository.findByLogin(login).orElseThrow(
                () -> new UsernameNotFoundException("Login: " + login + " not found"));
    }
	
}
