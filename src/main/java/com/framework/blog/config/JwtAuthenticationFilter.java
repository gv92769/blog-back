package com.framework.blog.config;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;

/**
*
* @author Gabriel Vinicius
*/
public class JwtAuthenticationFilter extends OncePerRequestFilter {
   
   @Autowired
   private JwtTokenProvider tokenProvider;
   
   @Override
   protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws ServletException, IOException {
       String token = tokenProvider.resolveToken(req);
       if(token != null){
           try{
               Authentication authentication = tokenProvider.parseAccessToken(token);
               SecurityContextHolder.getContext().setAuthentication(authentication);
           } catch(ExpiredJwtException e){
               sendError(res, new ErrorBody(HttpStatus.UNAUTHORIZED.value(),
                       "Unauthorized", "expired_token",req.getRequestURI()));
               return;
           } catch(JwtException e){
               sendError(res, new ErrorBody(HttpStatus.UNAUTHORIZED.value(),
                       "Unauthorized","invalid_token",req.getRequestURI()));
               return;
           } catch(Exception e){
               sendError(res, new ErrorBody(HttpStatus.UNAUTHORIZED.value(),
                       "Unauthorized","Error in authentication: token processing failed",
                       req.getRequestURI()));
               return;
           }
       }
       chain.doFilter(req, res);
   }
   
   private void sendError(HttpServletResponse res, ErrorBody errorBody) throws IOException {
       ObjectMapper mapper = new ObjectMapper();
       String body = mapper.writeValueAsString(errorBody);
       res.setContentType(MediaType.APPLICATION_JSON_VALUE);
       res.setStatus(errorBody.getStatus());
       res.getWriter().write(body);
   }
   
   public class ErrorBody {
       private final String timestamp;
       
       private final int status;
       
       private final String error;
       
       private final String message;
       
       private final String path;

       public ErrorBody(int status, String error, String message, String path) {
           this.timestamp = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME);
           this.status = status;
           this.error = error;
           this.message = message;
           this.path = path;
       }

       public String getTimestamp() {
           return timestamp;
       }

       public int getStatus() {
           return status;
       }

       public String getError() {
           return error;
       }

       public String getMessage() {
           return message;
       }

       public String getPath() {
           return path;
       }
    
   }
   
}
