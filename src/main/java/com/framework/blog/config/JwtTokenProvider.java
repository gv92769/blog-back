package com.framework.blog.config;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.crypto.SecretKey;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.framework.blog.entity.Usuario;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;

/**
*
* @author Gabriel Vinicius
*/
@Component
public class JwtTokenProvider {

	private static final String CLAIM_AUTHORITY = "authority";
	
    private static final String CLAIM_ID = "id";
    
    private static final String REFRESH_TOKEN_TYPE = "refresh_token";
    
    @Value("${security.jwt.secret}")
    private String jwtSecret;
    
    @Value("${security.jwt.accessTokenValidForInSeconds}")
    private Long jwtAccessTokenValidForInSeconds;
    
    @Value("${security.jwt.refreshTokenValidForInSeconds}")
    private Long jwtRefreshTokenValidForInSeconds;
    
    
    public String resolveToken(HttpServletRequest req) {
        String bearerToken = req.getHeader("Authorization");
        
        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7, bearerToken.length());
        }
        
        return null;
    }
    
    public String createAccessToken(UserDetails userDetails) {
        final String authorities = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(","));
        
        Claims claims = Jwts.claims().setSubject(userDetails.getUsername());
        claims.put(CLAIM_AUTHORITY, authorities);
        claims.put(CLAIM_ID, ((Usuario) userDetails).getId());

        Date now = new Date();
        Date validity = new Date(now.getTime() + jwtAccessTokenValidForInSeconds*1000L);
        
        SecretKey key = Keys.hmacShaKeyFor(DatatypeConverter.parseBase64Binary(jwtSecret));

        return Jwts.builder()//
            .setClaims(claims)//
            .setIssuedAt(now)//
            .setExpiration(validity)
            .signWith(key)//
            .compact();
    }
    
    public String createRefreshToken(UserDetails userDetails){        
        Claims claims = Jwts.claims().setSubject(userDetails.getUsername());
        claims.put(CLAIM_AUTHORITY, REFRESH_TOKEN_TYPE);
        claims.put(CLAIM_ID, ((Usuario) userDetails).getId());

        Date now = new Date();
        Date validity = new Date(now.getTime() + jwtRefreshTokenValidForInSeconds*1000L);
        
        SecretKey key = Keys.hmacShaKeyFor(DatatypeConverter.parseBase64Binary(jwtSecret));

        return Jwts.builder()//
            .setClaims(claims)//
            .setIssuedAt(now)//
            .setExpiration(validity)
            .signWith(key)//
            .compact();
    }
    
    public Authentication parseAccessToken(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(jwtSecret))
                .parseClaimsJws(token).getBody();

        String tokenSubject = claims.getSubject();
        String tokenAuthorities = (String) claims.get(CLAIM_AUTHORITY); 
        List<GrantedAuthority> tokenAuthoritiesList = AuthorityUtils.commaSeparatedStringToAuthorityList(tokenAuthorities);

        UsernamePasswordAuthenticationToken authentication
                = new UsernamePasswordAuthenticationToken(tokenSubject, null, tokenAuthoritiesList);

        return authentication;
    }
    
    public Authentication parseRefreshToken(String token){
        Claims claims = Jwts.parser()
                .require(CLAIM_AUTHORITY, REFRESH_TOKEN_TYPE)
                .setSigningKey(DatatypeConverter.parseBase64Binary(jwtSecret))
                .parseClaimsJws(token).getBody();

        String tokenSubject = claims.getSubject();

        UsernamePasswordAuthenticationToken authentication
                = new UsernamePasswordAuthenticationToken(tokenSubject, null, null);

        return authentication;
    } 
    
    public Long claimId(String token) {
    	Claims claims = Jwts.parser()
                .require(CLAIM_AUTHORITY, REFRESH_TOKEN_TYPE)
                .setSigningKey(DatatypeConverter.parseBase64Binary(jwtSecret))
                .parseClaimsJws(token).getBody();
    	
    	return Long.parseLong(claims.get(CLAIM_ID).toString());
    }
    
}
