package com.framework.blog.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
*
* @author Gabriel Vinicius
*/
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecutiryConfig extends WebSecurityConfigurerAdapter {

   @Autowired
   private CustomUserDetailsService customUserDetailsService;
   
   @Autowired
   private JwtAuthenticationEntryPoint unauthorizedHandler;
   
   @Bean
   @Override
   public AuthenticationManager authenticationManagerBean() throws Exception {
       return super.authenticationManagerBean();
   }

   @Override
   protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
       authenticationManagerBuilder
               .userDetailsService(customUserDetailsService)
               .passwordEncoder(passwordEncoder());
   }
   
   @Bean
   public PasswordEncoder passwordEncoder() {
       return new BCryptPasswordEncoder();
   }

   @Bean
   public JwtAuthenticationFilter authenticationFilterBean() {
       return new JwtAuthenticationFilter();
   }
   
   @Override
   protected void configure(HttpSecurity http) throws Exception {
       http
           .httpBasic().disable()
           .cors().and()
           .csrf().disable()
           .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
           .and()
           .exceptionHandling().authenticationEntryPoint(unauthorizedHandler)
           .and()
           .addFilterBefore(authenticationFilterBean(), UsernamePasswordAuthenticationFilter.class)
           .authorizeRequests().antMatchers("/autenticacao/**").permitAll()
           .and()
           .authorizeRequests().antMatchers("/usuario/**").permitAll()
           .anyRequest().authenticated();
   }
   
}
