package com.framework.blog.entity;

import com.fasterxml.jackson.annotation.JsonValue;

public enum PerfilUsuario {

	ROLE_ADMINISTRADOR("Administrador"),
    ROLE_USUARIO("Usuário");
    
    @JsonValue
    private final String nome;

    private PerfilUsuario(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public static PerfilUsuario getPerfilUsuarioPorNome(String nome){
        for(PerfilUsuario perfil : values()){
            if(perfil.getNome().equalsIgnoreCase(nome))
                return perfil;
        }
        return null;
    }
	
}
