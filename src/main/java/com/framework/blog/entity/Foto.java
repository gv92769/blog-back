package com.framework.blog.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "FOTO")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class Foto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2495255586609149090L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@JsonIgnore
	@ManyToOne(optional = false)
	@JoinColumn(name="album_id", referencedColumnName="id")
	private Album album;
	
	private String descricao;
	
}
