package com.framework.blog.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
*
* @author Gabriel Vinicius
*/
@Entity
@Table(name = "ALBUM")
@NoArgsConstructor @AllArgsConstructor @Getter @Setter
public class Album implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7392902942349948581L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="usuario_id", referencedColumnName="id")
	private Usuario usuario;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="album_id", referencedColumnName="id")
	private List<Foto> fotos;
	
	@Transient
	private List<MultipartFile> imagens;
	
	private String descricao;
	
	public Album(Long id) {
		this.id = id;
	}
	
}
